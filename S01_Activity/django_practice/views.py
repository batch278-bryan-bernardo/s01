from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse

# local imports
from .models import GroceryItem
# django import
from django.template import loader

# For Authentication/Register Imports

from django.contrib.auth.models import User

from django.contrib.auth import authenticate, login, logout

from django.contrib.auth.hashers import make_password

# For conversion of models to a dictionary

from django.forms.models import model_to_dict

# For forms imports

from .forms import LoginForm, RegisterForm, AddGroceryForm

from django.utils import timezone

# Create your views here.


def index(request):

    groceryitem_list = GroceryItem.objects.filter(user_id=request.user.id)
    context = {
        'groceryitem_list': groceryitem_list,
        'user': request.user
    }
    return render(request, "index.html", context)


def groceryitem(request, groceryitem_id):
    # response = f"This is your Grocery Item {groceryitem_id} Details"

    groceryitem = get_object_or_404(GroceryItem, pk=groceryitem_id)

    return render(request, 'groceryitem.html', model_to_dict(groceryitem))


def register(request):
    context = {}

    if request.method == "POST":
        form = RegisterForm(request.POST)

        if form.is_valid() == False:
            form = RegisterForm()

        else:

            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            email = form.cleaned_data['email']

        duplicates = User.objects.filter(username=username)

        if not duplicates:

            User.objects.create(username=username,
                                first_name=first_name,
                                last_name=last_name,
                                password=make_password(password),
                                email=email,
                                is_staff=False,
                                is_active=True,
                                date_joined=timezone.now()
                                )

            return redirect('groceryapp:login')

        else:
            context = {
                'error': True
            }

    return render(request, "register.html", context)


def change_password(request):

    context = {}

    if request.method == 'POST':

    user = authenticate(username="notadmin123", password="notadmin")

    if user is not None:

        authenticated_user = User.objects.get(username="notadmin123")
        authenticated_user.set_password('notadmin123')
        authenticated_user.save()
        is_user_authenticated = True

    context = {

        'is_user_authenticated': is_user_authenticated
    }

    return render(request, "change_password.html", context)


def login_user(request):

    context = {}

    if request.method == 'POST':
        form = LoginForm(request.POST)

        if form.is_valid() == False:
            form = LoginForm()
        else:
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = authenticate(username=username, password=password)

            if user is not None:
                context = {
                    'username': username,
                    'password': password
                }
                login(request, user)
                return redirect("groceryapp:index")
            else:
                context = {
                    'error': True
                }

    return render(request, "login.html", context)


def logout_user(request):
    logout(request)
    return redirect("groceryapp:index")


def add_grocery_item(request):
    context = {}

    if request.method == 'POST':
        form = AddGroceryForm(request.POST)

        if form.is_valid() == False:
            form = AddGroceryForm()

        else:
            item_name = form.cleaned_data['item_name']
            quantity = form.cleaned_data['quantity']
            category = form.cleaned_data['category']

        duplicates = GroceryItem.objects.filter(
            item_name=item_name, user_id=request.user.id)

        if not duplicates:
            # Creates an object based on the ToDoItem model and saves to the record in the database

            GroceryItem.objects.create(
                item_name=item_name, quantity=quantity, category=category, date_created=timezone.now(), user_id=request.user.id)

            return redirect("groceryapp:index")

        else:
            context = {
                'error': True
            }

    return render(request, 'add_grocery_item.html', context)
