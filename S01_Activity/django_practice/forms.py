from django import forms


class LoginForm(forms.Form):
    username = forms.CharField(label="username", max_length=20)
    password = forms.CharField(label="password", max_length=20)


class RegisterForm(forms.Form):
    username = forms.CharField(label="username", max_length=20)
    password = forms.CharField(label="password", max_length=20)
    email = forms.CharField(label="email", max_length=20)
    first_name = forms.CharField(label="first_name", max_length=20)
    last_name = forms.CharField(label="last_name", max_length=20)


class AddGroceryForm(forms.Form):
    item_name = forms.CharField(label="item_name", max_length=50)
    quantity = forms.CharField(label="quantity", max_length=50)
    category = forms.CharField(label="category", max_length=50)


class ChangePasswordForm(forms.Form):
    password = forms.CharField(label="password", max_length=50)
    new_password = forms.CharField(label="new_password", max_length=50)
    re_password = forms.CharField(label="re_password", max_length=50)
