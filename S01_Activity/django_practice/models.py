from django.db import models

from django.contrib.auth.models import User

# Create your models here.

# 1


class GroceryItem(models.Model):
    item_name = models.CharField(max_length=50)
    quantity = models.CharField(max_length=50, default="")
    category = models.CharField(max_length=50)
    status = models.CharField(max_length=50, default="pending")
    date_created = models.DateTimeField("date_created")
    user = models.ForeignKey(User, on_delete=models.RESTRICT, default=None)
