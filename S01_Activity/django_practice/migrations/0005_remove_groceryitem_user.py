# Generated by Django 4.1.7 on 2023-03-31 05:51

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('django_practice', '0004_groceryitem_user'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='groceryitem',
            name='user',
        ),
    ]
