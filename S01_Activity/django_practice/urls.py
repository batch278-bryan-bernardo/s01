from django.urls import path
from . import views


app_name = "groceryapp"

urlpatterns = [
    path('index', views.index, name='index'),
    path('<int:groceryitem_id>/', views.groceryitem, name="viewgroceryitem"),
    path('register', views.register, name="register"),
    path('change_password', views.change_password, name="change_password"),
    path('login', views.login_user, name="login"),
    path('logout', views.logout_user, name="logout"),
    path('add_grocery_item', views.add_grocery_item, name="add_grocery_item")
]
