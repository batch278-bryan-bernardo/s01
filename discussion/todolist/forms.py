# Getter of values for forms that is rendered in html/website

from django import forms


class LoginForm(forms.Form):
    username = forms.CharField(label="username", max_length=20)
    password = forms.CharField(label="password", max_length=20)


class AddTaskForm(forms.Form):
    task_name = forms.CharField(label="task_name", max_length=50)
    description = forms.CharField(label="description", max_length=200)


class UpdateTaskForm(forms.Form):
    task_name = forms.CharField(label="task_name", max_length=50)
    description = forms.CharField(label="description", max_length=200)
    status = forms.CharField(label="status", max_length=50)


class RegisterForm(forms.Form):
    username = forms.CharField(label="username", max_length=20)
    password = forms.CharField(label="password", max_length=20)
    con_password = forms.CharField(label="con_password", max_length=20)
    email = forms.CharField(label="email", max_length=20)
    first_name = forms.CharField(label="first_name", max_length=20)
    last_name = forms.CharField(label="last_name", max_length=20)


class UpdateUserForm(forms.Form):
    first_name = forms.CharField(
        label="first_name", max_length=50)
    last_name = forms.CharField(
        label="last_name", max_length=50)
    password = forms.CharField(label="password", max_length=20)
    new_password = forms.CharField(
        label="new_password", max_length=20, required=False)
    re_password = forms.CharField(
        label="re_password", max_length=20, required=False)


# Event Feature


class AddEventForm(forms.Form):
    event_name = forms.CharField(label="event_name", max_length=50)
    event_time = forms.CharField(label="event_time", max_length=50)
    description = forms.CharField(label="description", max_length=200)
