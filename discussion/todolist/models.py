from django.db import models

# Create your models here.
from django.contrib.auth.models import User


class ToDoItem(models.Model):
    task_name = models.CharField(max_length=50)
    description = models.CharField(max_length=200)
    status = models.CharField(max_length=50, default="pending")
    date_created = models.DateTimeField('date created')
    # For foreign key use models.ForeignKey(<Model_name>, other_arguements)
    user = models.ForeignKey(User, on_delete=models.RESTRICT, default="")


class EventItem(models.Model):
    event_name = models.CharField(max_length=50)
    event_time = models.CharField(max_length=50, default="")
    description = models.CharField(max_length=200)
    status = models.CharField(max_length=50, default="on going")
    date_created = models.DateTimeField('date created')
    # For foreign key use models.ForeignKey(<Model_name>, other_arguements)
    user = models.ForeignKey(User, on_delete=models.RESTRICT, default="")
