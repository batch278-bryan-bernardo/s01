from django.urls import path

# Import the views.py from the same folder
from . import views

app_name = 'todolist'

urlpatterns = [
    path('index', views.index, name='index'),
    path('task/<int:todoitem_id>/', views.todoitem, name="viewtodoitem"),
    path('register', views.register, name="register"),
    path('update_user', views.update_user, name="update_user"),
    path('login', views.login_user, name="login"),
    path('logout', views.logout_user, name="logout"),
    path('add_task', views.add_task, name="add_task"),
    path('<int:todoitem_id>/update_task',
         views.update_task, name="update_task"),
    path('<int:todoitem_id>/delete', views.delete_task, name="delete_task"),
    path('add_event', views.add_event, name="add_event"),
    path('event/<int:eventitem_id>/', views.eventitem, name="vieweventitem")

]
