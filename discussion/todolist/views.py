# For django shortcuts render, redirect
from django.shortcuts import render, redirect, get_object_or_404

from django.contrib.auth.hashers import make_password, check_password
# Create your views here.

# The from keyword allows importing of necessary classes/modules, methods and other files needed in our application from the django.http package while the import keyword defines what we are importing from the package.

from django.http import HttpResponse

# Local imports
from .models import ToDoItem, EventItem

# import for the built in User Model

from django.contrib.auth.models import User

# to use the template created:
# from django.template import loader

# import for the built in Authenticate Function, Login Function, Logout Function
from django.contrib.auth import authenticate, login, logout

# import for models changing model to a dictionary data
from django.forms.models import model_to_dict

# import for todolis forms

from .forms import LoginForm, AddTaskForm, UpdateTaskForm, RegisterForm, UpdateUserForm

# import for event forms

from .forms import AddEventForm

# django utilities

from django.utils import timezone


def index(request):
    # ToDoItem and EventItem
    todoitem_list = ToDoItem.objects.filter(user_id=request.user.id)
    eventitem_list = EventItem.objects.filter(user_id=request.user.id)
    # template = loader.get_template("index.html") - Long cut
    context = {
        'todoitem_list': todoitem_list,
        'eventitem_list': eventitem_list,
        "user": request.user
    }
    # output = ", ".join([todoitem.task_name for todoitem in todoitem_list])
    # render(request, route_template, context)
    return render(request, "index.html", context)


def todoitem(request, todoitem_id):
    # response = f"You are viewing the details of {todoitem_id}"
    # return HttpResponse(response)

    todoitem = get_object_or_404(ToDoItem, pk=todoitem_id)
    return render(request, "todoitem.html", model_to_dict(todoitem))

# This function is responsible for registering on our application.


def register(request):

    context = {}

    if request.method == "POST":
        form = RegisterForm(request.POST)

        if form.is_valid() == False:
            form = RegisterForm()

        else:
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            con_password = form.cleaned_data['con_password']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            email = form.cleaned_data['email']

        duplicates = User.objects.filter(username=username)

        if not duplicates and (con_password == password):

            User.objects.create(username=username,
                                first_name=first_name,
                                last_name=last_name,
                                password=make_password(password),
                                email=email,
                                is_staff=False,
                                is_active=True,
                                date_joined=timezone.now()
                                )

            return redirect('todolist:login')

        else:
            context = {
                'error': True
            }

    return render(request, "register.html", context)


def update_user(request):

    context = {}

    user = get_object_or_404(User, pk=request.user.id)

    context = {
        'first_name': user.first_name,
        'last_name': user.last_name
    }

    if request.method == 'POST':
        form = UpdateUserForm(request.POST)

        if form.is_valid() == False:
            form = UpdateUserForm()
        else:
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            password = form.cleaned_data['password']
            new_password = form.cleaned_data['new_password']
            re_password = form.cleaned_data['re_password']

            if user and check_password(password, request.user.password):
                user.first_name = first_name
                user.last_name = last_name
                if new_password and re_password and (new_password == re_password):
                    user.password = make_password(new_password)
                    user = authenticate(username=user.username,
                                        password=new_password)
                    login(request, user)

                user.save()
                return redirect('todolist:index')
            else:
                context = {
                    'error': True
                }

    return render(request, "update_user.html", context)


def login_user(request):

    context = {}

    # if this is a POST request we nee to process the form data

    if request.method == "POST":
        form = LoginForm(request.POST)

        if form.is_valid() == False:
            form = LoginForm()
        else:
            # cleaned_data is a method to get/retrieves the properties or information that was entered in the form.
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = authenticate(username=username, password=password)

            if user is not None:
                context = {
                    'username': username,
                    'password': password
                }
                login(request, user)
                return redirect("todolist:index")
            else:
                context = {
                    'error': True
                }

    return render(request, "login.html", context)
    # username = form.username
    # password = 'jane1234'

    # user = authenticate(username=username, password=password)

    # if user is not None:
    #     login(request, user)
    #     return redirect("todolist:index")


def logout_user(request):
    logout(request)
    return redirect("todolist:index")


def add_task(request):
    context = {}

    if request.method == 'POST':
        form = AddTaskForm(request.POST)

        if form.is_valid() == False:
            form = AddTaskForm()

        else:
            task_name = form.cleaned_data['task_name']
            description = form.cleaned_data['description']

        duplicates = ToDoItem.objects.filter(
            task_name=task_name, user_id=request.user.id)

        if not duplicates:
            # Creates an object based on the ToDoItem model and saves to the record in the database

            ToDoItem.objects.create(
                task_name=task_name, description=description, date_created=timezone.now(), user_id=request.user.id)

            return redirect("todolist:index")

        else:
            context = {
                'error': True
            }

    return render(request, 'add_task.html', context)


def update_task(request, todoitem_id):

    todoitem = ToDoItem.objects.filter(pk=todoitem_id)

    context = {
        "user": request.user,
        'todoitem_id': todoitem_id,
        "task_name": todoitem[0].task_name,
        "description": todoitem[0].description,
        "status": todoitem[0].status
    }

    if request.method == 'POST':
        form = UpdateTaskForm(request.POST)

        if form.is_valid() == False:
            form = UpdateTaskForm()

        else:
            task_name = form.cleaned_data['task_name']
            description = form.cleaned_data['description']
            status = form.cleaned_data['status']

            if todoitem:
                todoitem[0].task_name = task_name
                todoitem[0].description = description
                todoitem[0].status = status

                todoitem[0].save()

                return redirect("todolist:viewtodoitem", todoitem_id=todoitem[0].id)

            else:
                context = {
                    'error': True
                }
    return render(request, "update_task.html", context)


def delete_task(request, todoitem_id):

    ToDoItem.objects.filter(pk=todoitem_id).delete()

    return redirect("todolist:index")


# Events


def add_event(request):
    context = {}

    if request.method == 'POST':
        form = AddEventForm(request.POST)

        if form.is_valid() == False:
            form = AddEventForm()

        else:
            event_name = form.cleaned_data['event_name']
            event_time = form.cleaned_data['event_time']
            description = form.cleaned_data['description']

        duplicates = EventItem.objects.filter(
            event_name=event_name, user_id=request.user.id)

        if not duplicates:
            # Creates an object based on the ToDoItem model and saves to the record in the database

            EventItem.objects.create(
                event_name=event_name, event_time=event_time, description=description, date_created=timezone.now(), user_id=request.user.id)

            return redirect("todolist:index")

        else:
            context = {
                'error': True
            }

    return render(request, 'add_event.html', context)


def eventitem(request, eventitem_id):

    eventitem = get_object_or_404(EventItem, pk=eventitem_id)

    return render(request, "eventitem.html", model_to_dict(eventitem))
